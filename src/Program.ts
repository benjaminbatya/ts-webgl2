import * as utils from './utils'
import { vec3, vec4, mat4 } from 'gl-matrix'


export default class Program {

  gl : WebGL2RenderingContext;
  program : WebGLProgram;

  attributes = new Map<string, number>();
  uniforms = new Map<string, WebGLUniformLocation>();

  constructor(gl:WebGL2RenderingContext , VertShader:string, FragShader:string) {
    this.gl = gl;
  
    const vertShader = utils.buildShader(gl, utils.ShaderType.Vertex, VertShader)
    const fragShader = utils.buildShader(gl, utils.ShaderType.Fragment, FragShader)

    this.program = gl.createProgram();
    gl.attachShader(this.program, vertShader);
    gl.attachShader(this.program, fragShader);
    gl.linkProgram(this.program);

    if(!gl.getProgramParameter(this.program, gl.LINK_STATUS)) {
      const info = this.gl.getProgramInfoLog(this.program);
      throw new Error(`could not initialize shaders: \n${info}`)
    }

    gl.useProgram(this.program)
  }

  attribLocation(name:string) {
    let attrib = this.attributes.get(name);
    if(!attrib) {
      attrib = this.gl.getAttribLocation(this.program, name);
      this.attributes.set(name, attrib);
    }
    return attrib;
  }

  uniformLocation(name:string) {
    let uniform = this.uniforms.get(name);
    if(!uniform) {
      uniform = this.gl.getUniformLocation(this.program, name);
      this.uniforms.set(name, uniform);
    }
    return uniform;
  }

  uniform1f(name:string, value:number) {
    const loc = this.uniformLocation(name);
    this.gl.uniform1f(loc, value);
  }

  uniform3fv(name:string, value:vec3) {
    const loc = this.uniformLocation(name);
    this.gl.uniform3fv(loc, value);  
  }

  uniform4fv(name:string, value:vec4) {
    const loc = this.uniformLocation(name);
    this.gl.uniform4fv(loc, value);  
  }

  uniformMatrix4fv(name:string, value:mat4, transpose = false) {
    const loc = this.uniformLocation(name);
    this.gl.uniformMatrix4fv(loc, transpose, value);
  }

  bind(fn:()=>void) {
    try {
      // this.gl.useProgram(this.program)

      fn();

    } finally {
      // this.gl.useProgram(null);
    }
  }
}