import './normalize.css';

import App from './App'

function findCanvas() {
  const canvas = document.getElementById('webgl-canvas') as HTMLCanvasElement;
  // canvas.id = 'webgl-canvas';
  canvas.width = 800;
  canvas.height = 600;

  // canvas.innerHTML = "Your browser does not support the HTML5 canvas element."

  return canvas;
}

const canvas = findCanvas()

document.body.appendChild(canvas);

const app = new App(canvas)
app.init();