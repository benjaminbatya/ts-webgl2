import * as utils from './utils'

import VertShader from './shaders/goraud.vert'
import FragShader from './shaders/goraud.frag'

import { vec3, vec4, mat4 } from 'gl-matrix'
import Model, {IModel} from './Model'
import Program from './Program'

export default class App {

  gl : WebGL2RenderingContext;
  program : Program;

  isAnimating = true;
  lastTime = 0;
  // If set true, we use the `LINES` drawing primitive instead of `TRIANGLES`
  wireframe = false;
  angle = 0;

  shininess = 10.0;
  clearColor : utils.Color = [0.9, 0.9, 0.9, 1];
  lightDiffuse : utils.Color = [1, 1, 1, 1];
  lightAmbient : utils.Color = [0.03, 0.03, 0.03, 1];
  lightSpecular : utils.Color = [1, 1, 1, 1];
  lightDirection : vec3 = [-0.25, -0.25, -0.25];
  materialDiffuse : utils.Color = [46 / 256, 99 / 256, 191 / 256, 1];
  materialAmbient : utils.Color = [1, 1, 1, 1];
  materialSpecular : utils.Color = [1, 1, 1, 1];

  parts : Model[] = [];

  projectionMatrix = mat4.create();
  modelViewMatrix = mat4.create();
  normalMatrix = mat4.create();

  constructor(canvas : HTMLCanvasElement) {
    // console.log(canvas)
    const gl = canvas.getContext('webgl2')
    this.gl = gl
    if(!gl) {
      throw new Error("no context available")
    }

    // utils.autoResizeCanvas(canvas, this.draw.bind(this))
  }

  async init() {
    const gl = this.gl;
    gl.clearColor(...this.clearColor);
    gl.clearDepth(100)
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL)

    this.initProgram();
    await this.initGeometry();

    this.initControls();
    
    // start the animation loop
    this.render();
  }

  initProgram() {

    const gl = this.gl;

    this.program = new Program(gl, VertShader, FragShader)
  }

  async initGeometry() {
    const gl = this.gl;

    const promises : Array<Promise<Model>> = [];

    for(let idx=1; idx<=150;++idx) {
      const part = new Model(gl);

      const promise = part.load(`/audi-r8/part${idx}.json`)
        .then(model => {
          
          // setup the vertex position attribute
          model.configureVBO((data:IModel) => {
            gl.enableVertexAttribArray(this.program.attribLocation('aVertexPosition'))
            gl.vertexAttribPointer(this.program.attribLocation('aVertexPosition'), 3, gl.FLOAT, false, 0, 0);
          })

          // setup the vertex normal attribute
          model.configureNormals((data:IModel) => {
            gl.enableVertexAttribArray(this.program.attribLocation('aVertexNormal'));
            gl.vertexAttribPointer(this.program.attribLocation('aVertexNormal'), 3, gl.FLOAT, false, 0, 0);
          })

          return model;
        })

      promises.push(promise)
      this.parts.push(part)
    }

    await Promise.all(promises)  
  }

  draw() {
    try {
      const gl = this.gl;

      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
      gl.viewport(0, 0, gl.canvas.width, gl.canvas.height)

      // transforms
      // We will cover these operations in later chapters
      mat4.perspective(this.projectionMatrix, 45, gl.canvas.width / gl.canvas.height, 0.1, 10000);

      mat4.identity(this.modelViewMatrix);
      mat4.translate(this.modelViewMatrix, this.modelViewMatrix, [-10, 0, -100]);
      mat4.scale(this.modelViewMatrix, this.modelViewMatrix, [20, 20, 20])
      mat4.rotate(this.modelViewMatrix, this.modelViewMatrix, 30 * Math.PI / 180, [1, 0, 0]);
      mat4.rotate(this.modelViewMatrix, this.modelViewMatrix, 30 * Math.PI / 180, [0, 1, 0]);
      // New operation for rotating the Model-View matrix
      mat4.rotate(this.modelViewMatrix, this.modelViewMatrix, this.angle * Math.PI / 180, [0, 1, 0]);

      mat4.identity(this.normalMatrix);
      mat4.translate(this.normalMatrix, this.normalMatrix, [-10, 0, -100]);
      // mat4.copy(this.normalMatrix, this.modelViewMatrix);
      mat4.invert(this.normalMatrix, this.normalMatrix);
      mat4.transpose(this.normalMatrix, this.normalMatrix);

      this.program.bind(() => {
        // update the uniforms
        this.program.uniformMatrix4fv('uProjectionMatrix', this.projectionMatrix);
        this.program.uniformMatrix4fv('uModelViewMatrix', this.modelViewMatrix);
        this.program.uniformMatrix4fv('uNormalMatrix', this.normalMatrix);
        
        this.program.uniform3fv('uLightDirection', [-this.lightDirection[0], - this.lightDirection[1], this.lightDirection[2]]);
        this.program.uniform4fv('uLightAmbient', this.lightAmbient);
        this.program.uniform4fv('uLightDiffuse', this.lightDiffuse);
        this.program.uniform4fv('uLightSpecular', this.lightSpecular);

        this.program.uniform4fv('uMaterialAmbient', this.materialAmbient);
        this.program.uniform4fv('uMaterialDiffuse', this.materialDiffuse);
        this.program.uniform4fv('uMaterialSpecular', this.materialSpecular);
        this.program.uniform1f('uShininess', this.shininess);

        this.parts.forEach(part => {
          // bind the buffers
          part.bind((data:IModel) => {
            // Draw

            if(this.wireframe) {
              gl.drawElements(gl.LINES, data.indices.length, gl.UNSIGNED_SHORT, 0);
            } else {
              gl.drawElements(gl.TRIANGLES, data.indices.length, gl.UNSIGNED_SHORT, 0);
            }
          });
        })
      })

    } catch(err) {
      console.error(err)
    }
  }

  animate() {
    const now = Date.now();
    if(this.lastTime && this.isAnimating) {
      const elapsed = now - this.lastTime;
      this.angle += (90 * elapsed) / 1000.0
    }
    this.lastTime = now;
  }

  // Although we don't necessarily need to call `draw` on every
  // rendering cycle in this example, we will soon cover why this is important
  render() {
    requestAnimationFrame(this.render.bind(this));
    this.animate();
    this.draw();
  }

  initControls() {

    const gl = this.gl;

    const controls = {
      'Animate': {
        value : this.isAnimating,
        onChange : (v: boolean) => {
          this.isAnimating = v;
        }
      },
      'Wireframe': {
        value : this.wireframe,
        onChange : (v: boolean) => {
          this.wireframe = v;
        }
      },
      'Light Diffuse Color': {
        value: utils.denormalizeColor(this.lightDiffuse),
        onChange : (v: utils.Color) => {
          this.lightDiffuse = utils.normalizeColor(v);
        }
      },
      'Light Ambient Term': {
        value: this.lightAmbient[0],
        min: 0, max: 1, step: 0.01,
        onChange: (v:number) => this.lightAmbient = [v, v, v, 1]
      },
      'Light Specular Term': {
        value: this.lightSpecular[0],
        min: 0, max: 1, step: 0.01,
        onChange: (v:number) => this.lightSpecular = [v, v, v, 1]
      },
      'Material Color': {
        value: utils.denormalizeColor(this.materialDiffuse),
        onChange : (v: utils.Color) => {
          this.materialDiffuse = utils.normalizeColor(v);
        }
      },
      'Material Ambient Term': {
        value: this.materialAmbient[0],
        min: 0, max: 1, step: 0.01,
        onChange: (v:number) => this.materialAmbient = [v, v, v, 1]
      },
      'Material Specular Term': {
        value: this.materialSpecular[0],
        min: 0, max: 1, step: 0.01,
        onChange: (v:number) => this.materialSpecular = [v, v, v, 1]
      },
      Shininess: {
        value: this.shininess,
        min: 0, max: 50, step: 0.1,
        onChange: (v:number) => this.shininess = v
      },
      Background: {
        value: utils.denormalizeColor(this.clearColor),
        onChange: (v:utils.Color) => gl.clearColor(...utils.normalizeColor(v))
      },
    }

    Object.assign(controls, 
      ['Translate X', 'Translate Y', 'Translate Z'].reduce((result:any, name, i) => {
        result[name] = {
          value: this.lightDirection[i],
          min: -10, max: 10, step: -0.1,
          onChange: (v:number, state:any) => {
            this.lightDirection = [
              state['Translate X'],
              state['Translate Y'],
              state['Translate Z']
            ]
          }
        }
        return result;
      }, {})
    );

    utils.configureControls(controls)
  }

}