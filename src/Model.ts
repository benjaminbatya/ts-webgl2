import * as utils from './utils'

export interface IModel {
  color : [number, number, number];
  vertices : number[];
  indices : number[];
}

export default class Model {

  readonly gl : WebGL2RenderingContext;
  
  data : IModel;
  VAO : WebGLVertexArrayObject;
  VBO : WebGLBuffer;
  normalsBuffer : WebGLBuffer;
  indexBuffer : WebGLBuffer;  

  constructor(  gl : WebGL2RenderingContext) {
    this.gl = gl;
  }

  release() {
    // Clean up all of the buffers
  }

  async load(filePath : string) {
    try {
      // console.log('fetching', filePath)
      const res = await fetch(filePath)

      // console.log('parsing', filePath)
      const data = await res.json();

      // console.log('loading', filePath)
      this._loadModel(data as IModel)
      return this;
    } catch(err) {
      console.error(err)
      throw new Error(err)
    }
  }

  _loadModel(model : IModel) {
    const gl = this.gl;

    this.data = model
    
    const normals = utils.calculateNormals(model.vertices, model.indices)

    // Setup VAO
    this.VAO = gl.createVertexArray();
    gl.bindVertexArray(this.VAO);

    // Setup VBO
    this.VBO = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.VBO);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(model.vertices), gl.STATIC_DRAW);
    
    // normals
    this.normalsBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.normalsBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normals), gl.STATIC_DRAW);

    // Setup IBO
    this.indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer)
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(model.indices), gl.STATIC_DRAW)

    // cleanup
    gl.bindBuffer(gl.ARRAY_BUFFER, null)
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null)
    gl.bindVertexArray(null);
  }

  configureVBO(fn:Function) {
    const gl = this.gl;

    try {
      gl.bindVertexArray(this.VAO)
      gl.bindBuffer(gl.ARRAY_BUFFER, this.VBO)
      fn(this.data)
    } finally {
      gl.bindBuffer(gl.ARRAY_BUFFER, null)
      gl.bindVertexArray(null);

    }
  }

  configureNormals(fn:Function) {
    const gl = this.gl;

    try {
      gl.bindVertexArray(this.VAO)
      gl.bindBuffer(gl.ARRAY_BUFFER, this.normalsBuffer)
      fn(this.data)
    } finally {
      gl.bindBuffer(gl.ARRAY_BUFFER, null)
      gl.bindVertexArray(null);

    }
  }

  bind(fn:Function) {
    try {
      this.gl.bindVertexArray(this.VAO)
      this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer)

      fn(this.data);

    } finally {
      this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, null)
      this.gl.bindVertexArray(null)
    }
  }
}