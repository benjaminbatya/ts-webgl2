const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  entry: './src/index.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(vert|frag)$/i,
        use: [
          {
            loader: 'raw-loader',
            options: {
              esModule: false,
            }
          }
        ]
      },
    ],
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Real-Time 3D Graphics with WebGL2',
      template: 'src/index.html.template',
    }),
    new webpack.SourceMapDevToolPlugin({}),
    new CleanWebpackPlugin(),
  ],
  devServer: {
    contentBase: './assets',
    hot: true,
    publicPath: '/',
  },
};